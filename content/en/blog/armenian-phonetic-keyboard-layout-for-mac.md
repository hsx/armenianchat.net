+++ 
draft = false
date = 2022-01-03T20:16:58-08:00
title = "Armenian Phonetic Keyboard Layout for Mac"
slug = "armenian-phonetic-keyboard-layout-for-mac" 
tags = []
categories = []
thumbnail = "akfm/8.png"
description = ""
+++

Armenian Phonetic Keyboard Layout which comes with Mac OSX is very hard to use, at least for me. I have created this layout for my personal use and I believe this is the most convenient layout, due to similarities with widely used Armenian Keyboard Layouts in operating systems and devices other than Mac OSX.

## Keyboard Layout
![armenian keyboard layout](/aklfm/k1.png)

**SHIFT** or **CAPS LOCK**  
![armenian keyboard layout for option or caps lock](/aklfm/k2.png)

**OPTION**  
![armenian keyboard layout for option](/aklfm/k3.png)

**OPTION + SHIFT** or **OPTION + CAPS LOCK**  
![armenian keyboard layout for option + shift or option + caps lock](/aklfm/k4.png)


## License
This software is released under [MIT Licence](https://opensource.org/licenses/MIT). 


## Installation (method 1)
- [Download Armenian Keyboard Layout](/aklfm/ArmenianPhoneticKeyboard.zip) bundle archive and put it on your Desktop
- Double click the ArmenianPhoneticKeyboard.zip to extract it
- Open Finder (Option + Command + Space) and do Go > Go To Folder... (Sift + Command + G), enter "~/Library/Keyboard Layouts/" and click "Go"

![Library folder with open go to the folder prompt](/aklfm/1.png)

- Drag and drop ArmenianPhoneticKeyboard.bundle file from your "Desktop" to "Keyboard Layouts" folder

![keyboard layouts folder with armenianphonetickeyboard file](/aklfm/2.png)

- Open System Preferences > Language & Region and click "+" to add Armenian in the "Preferred Languages" list. 

![language & region settings window](/aklfm/3.png) ![select a preferred language to add prompt](/aklfm/4.png)

- Select your primary language (click "Use English" or whatever language you currently have there if it is your primary language)

![primary language selection prompt](/aklfm/5.png)

- Select "Armenian - Phonetic" source and click "Add Input Source"

![select input source to add prompt](/aklfm/6.png)

- Congratulations! Now you can select the keyboard layout you want to use. You can also use "**Control + Space**" shortcut for switching the language on Mac.
- Don't forget to restart your computer.

![language selection menue on taskbar](/aklfm/8.png)

&nbsp; 
&nbsp; 
&nbsp; 

## Installation (method 2)
- Open System Preferences > Language & Region and click the “Keyboard Preferences” button. Click “+” to add a new input source.

![keyboard settings window](/aklfm/71.png)

- Select "Armenian" > "Armenian - Phonetic" source and click "Add Input Source"

![keyboard input source selection prompt](/aklfm/72.png)

- Congratulations! You have finished the phonetic layout installation. Don't forget to restart your computer.

![keyboard settings window with installed armenian phonetic keyboard layout](/aklfm/73.png)