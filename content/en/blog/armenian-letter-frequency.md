+++ 
draft = false
date = 2022-01-04T12:31:56-08:00
title = "Armenian Letter Frequency"
slug = "" 
tags = []
categories = []
thumbnail = "img/aclogo.png"
description = ""
+++

This is a little research about Armenian letter frequency in texts. As an example, I used the classic story of [Antoine de Saint-Exupéry "The Little Prince"](https://grapaharan.org/%D5%93%D5%B8%D6%84%D6%80%D5%AB%D5%AF_%D4%BB%D5%B7%D5%AD%D5%A1%D5%B6%D5%A8). There is a little margin of error, because of Armenian letters "Ու" and "Եվ", which are a combination of 2 parts.

Here are the charts:

{{< columns two >}}
{{< column >}}{{< figure src="/alf/letter_frequency.png" width="600" alt="frequency chart for combined letters" >}}{{< /column >}}
{{< column >}}{{< figure src="/alf/combined_letter_frequency.png" width="600" alt="frequency chart for combined letters" >}}{{< /column >}}
{{< /columns >}}

## Armenian vs English: Letter frequency distribution

Let's compare letter frequency for the top 10 most common letters between Armenian and English ([source](https://www.rd.com/article/common-letters-english-language/)) languages. 


|     | Armenian |   %   | English |   %   |
| :-: | :-----:  | :---: | :-----: | :---: |
|  1  |  **Ա**  | 12.89  | **E**  | 11.16  |
|  2  |  **Ն**  |  7.71  | **A**  |  8.50  |
|  3  |  **Ր**  |  7.16  | **R**  |  7.58  |
|  4  |  **Ե**  |  6.79  | **I**  |  7.54  |
|  5  |  **Ո**  |  6.38  | **O**  |  7.16  |
|  6  |  **Ի**  |  5.63  | **T**  |  6.95  |
|  7  |  **Մ**  |  4.02  | **N**  |  6.65  |
|  8  |  **Ս**  |  3.35  | **S**  |  5.73  |
|  9  |  **Ու** |  3.13  | **L**  |  5.49  |
|  10 |  **Կ**  |  3.03  | **C**  |  4.54  |
|     |         | 57.06 |         | 71.30 |